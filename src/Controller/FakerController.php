<?php

namespace App\Controller;

use Faker\Factory;
use Symfony\Component\HttpFoundation\Response;

class FakerController
{

    /*
     *  @Route("/Faker/FakerName")
     */

    public function FakerName(): Response
    {
        $faker = Factory::create();

        return new Response($faker->name);
    }

    /*
    *  @Route("/Faker/FakerPwd")
    */

    public function FakerPwd(): Response
    {
        $faker = Factory::create();

        return  new Response($faker->password);
    }

    /*
    *  @Route("/Faker/FakerLaP")
    */

    public function FakerLaP(): Response
    {
        $faker = Factory::create();

        return new Response("Random name - " . $faker->name . " and random password - " . $faker->password);
    }
}