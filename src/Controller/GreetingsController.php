<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class GreetingsController
{
    /*
     *  @Route("/Greetings/HelloWorld?test=Hello")
     */

    public function HelloWorld(Request $request)
    {
        $test = $request->get('test');

        return new Response($test);
    }
}